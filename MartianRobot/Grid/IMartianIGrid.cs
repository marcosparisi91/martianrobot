﻿using MartianRobot.Enums;
using MartianRobot.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace MartianRobot.Grid
{
    public interface IMartianIGrid
    {
        void SetGridMaxCoordinates(int maxX, int maxÝ);
        void SetLostRobot(Position position);
        bool IsValidPosition(Position position);
        bool FeelFallenRobotScent(Position position);
    }
}
