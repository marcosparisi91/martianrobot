﻿using MartianRobot.Enums;
using MartianRobot.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MartianRobot.Grid
{
    public class MartianGrid : IMartianIGrid
    {
        private static int _maxX;
        private static int _maxY;
        private List<Position> _lostRobot = new List<Position>();

        public MartianGrid()
        {
        }
        public void SetGridMaxCoordinates(int maxX, int maxY)
        {
            _maxX = maxX;
            _maxY = maxY;
        }

        public bool IsValidPosition(Position position)
        {
            return position.posX >=0 && position.posX <= _maxX && position.posY >= 0 && position.posY <= _maxY;
        }
                

        public void SetLostRobot(Position position)
        {
            _lostRobot.Add(position);
        }

        public bool FeelFallenRobotScent(Position position)
        {
            return _lostRobot.Any(_ => _.direction == position.direction && _.posX == position.posX && _.posY == position.posY);
        }

    }
}
