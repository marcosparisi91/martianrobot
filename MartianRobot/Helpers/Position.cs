﻿using MartianRobot.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MartianRobot.Helpers
{
    public class Position
    {
        public int posX { get; set; }
        public int posY { get; set; }
        public DirectionEnum direction { get; set; }
        public bool isLost { get; set; } = false;
        public void ParsePosition(string pos)
        {
            // USAR REGEX?
            var position = pos.Split(" ");

            // VALIDAR Y TIRAR ERROR SI NO ES CORRECTO
            posX = int.Parse(position[0]);
            posY = int.Parse(position[1]);
            direction = (DirectionEnum)Enum.Parse(typeof(DirectionEnum), position[2]);

        }

        public string GetPositionString()
        {
            return $"{posX} {posY} {direction.ToString()} " + (isLost ? "LOST" : string.Empty);
        }
    }
}
