﻿using MartianRobot.Enums;
using MartianRobot.Helpers;
using MartianRobot.Instructions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MartianRobot.Robots
{
    public class Robot : IRobot
    {


        private Position currentPosition = new Position();

        public void ExecuteInstruction(IInstruction instruction)
        {
           currentPosition = instruction.Execute();
        }

        public Position GetLastPosition()
        {
            return currentPosition;
        }

        public string GetParsedLastPosition()
        {
            return currentPosition.GetPositionString();
        }
    }
}
