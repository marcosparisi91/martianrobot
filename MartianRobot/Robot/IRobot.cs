﻿using MartianRobot.Helpers;
using MartianRobot.Instructions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MartianRobot.Robots
{
    public interface IRobot
    {
        void ExecuteInstruction(IInstruction instruction);
        Position GetLastPosition();
        string GetParsedLastPosition();
    }
}
