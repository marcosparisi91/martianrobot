﻿using MartianRobot.Enums;
using MartianRobot.Grid;
using MartianRobot.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace MartianRobot.Instructions
{
    public class MoveToInstruction : IInstruction
    {

        private IMartianIGrid martianIGrid;
        private string _instructionsString;
        Position _currentPosition;
        public MoveToInstruction(IMartianIGrid grid, string instructionsString, Position position)
        {
            martianIGrid = grid;
            _instructionsString = instructionsString;
            _currentPosition = position;
        }

        public Position Execute()
        {
            foreach (char inst in _instructionsString)
            {
                var lastPosition = CopyPosition();

                switch (inst)
                {
                    case 'R':
                    case 'L':
                        SetDirection(inst);
                        break;
                    case 'F':
                        if (!IsAboutToFall())
                        {
                            SetCoordenates(_currentPosition);
                        }
                        break;
                }

                if (!martianIGrid.IsValidPosition(_currentPosition))
                {
                    _currentPosition = lastPosition;
                    martianIGrid.SetLostRobot(_currentPosition);
                    _currentPosition.isLost = true;
                    return _currentPosition;
                }
            }

            return _currentPosition;
        }

        private void SetDirection(char dir)
        {
            if (dir == 'R')
            {
                if ((int)_currentPosition.direction < 3)
                {
                    _currentPosition.direction++;
                }
                else
                {
                    _currentPosition.direction = (DirectionEnum)0;
                }
            }
            else
            {
                if ((int)_currentPosition.direction > 0)
                {
                    _currentPosition.direction--;
                }
                else
                {
                    _currentPosition.direction = (DirectionEnum)3;
                }
            }

        }

        private void SetCoordenates(Position position)
        {

            switch ((int)position.direction)
            {
                case 0:
                    position.posY++;
                    break;
                case 1:
                    position.posX++;
                    break;
                case 2:
                    position.posY--;
                    break;
                case 3:
                    position.posX--;
                    break;
                default:
                    break;
            }
        }

        private bool IsAboutToFall()
        {
            return martianIGrid.FeelFallenRobotScent(_currentPosition);
        }

        private Position CopyPosition()
        {
            return new Position()
            {
                direction = _currentPosition.direction,
                isLost = _currentPosition.isLost,
                posX = _currentPosition.posX,
                posY = _currentPosition.posY
            };
        }
    }
}
