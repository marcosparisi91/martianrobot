﻿using MartianRobot.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace MartianRobot.Instructions
{
    public interface IInstruction
    {
        Position Execute();
    }
}
