﻿using MartianRobot.Grid;
using MartianRobot.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace MartianRobot.Instructions
{
    public class SetInitialPositionInstruction : IInstruction
    {
        private IMartianIGrid martianIGrid;
        private string _instructionsString;
        public SetInitialPositionInstruction(IMartianIGrid grid, string instructionsString)
        {
            martianIGrid = grid;
            _instructionsString = instructionsString;
        }
        public Position Execute()
        {
            var position = new Position();
            position.ParsePosition(_instructionsString);
            if (martianIGrid.IsValidPosition(position))
            {
                return position;
            }
            else
            {
                throw new Exception("Invalid position");
            }
        }
    }
}
