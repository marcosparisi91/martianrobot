﻿using MartianRobot.Grid;
using MartianRobot.Instructions;
using MartianRobot.Robots;
using System;
using System.Collections.Generic;

namespace MartianRobot
{
    class Program
    {
        private static Dictionary<IRobot, List<string>> robots = new Dictionary<IRobot, List<string>>();
        private static MartianGrid martianGrid = new MartianGrid();
        static void Main(string[] args)
        {
            martianGrid.SetGridMaxCoordinates(5, 3);

            robots.Add(new Robot(), new List<string>() { "1 1 E", "RFRFRFRF" });
            robots.Add(new Robot(), new List<string>() { "3 2 N", "FRRFLLFFRRFLL" });
            robots.Add(new Robot(), new List<string>() { "0 3 W", "LLFFFLFLFL" });

            ProcessRobots();

            ShowResults();
        }

        public static void ProcessRobots()
        {
            foreach (IRobot robot in robots.Keys)
            {
                var listInstruction = robots[robot];

                var initialInstruction = new SetInitialPositionInstruction(martianGrid, listInstruction[0]);

                robot.ExecuteInstruction(initialInstruction);

                var moveInstructions = new MoveToInstruction(martianGrid, listInstruction[1], robot.GetLastPosition());

                robot.ExecuteInstruction(moveInstructions);

            }
        }

        public static void ShowResults()
        {
            foreach (IRobot robot in robots.Keys)
            {
                Console.WriteLine( robot.GetParsedLastPosition());
            }
        }

    }
}
