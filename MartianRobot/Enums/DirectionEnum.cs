﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MartianRobot.Enums
{
    public enum DirectionEnum
    {
        N = 0,
        E = 1, 
        S = 2,
        W = 3
    }
}
